
include ./etc/help.mk

DEV_TOOLS_DOCKER_IMAGE ?= registry.gitlab.com/$(shell basename $(shell dirname $(PWD)))/kubernetes-the-customizable-way-tools:latest

.PHONY: build
build: ##@development build DEV_TOOLS_DOCKER_IMAGE
	docker build --pull -t ${DEV_TOOLS_DOCKER_IMAGE} .
